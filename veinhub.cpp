#include "veinhub.h"

#include "veinentity.h"
#include "veinpeer.h"

#include <veincore.pb.h>

#include <QDebug>



VeinHub::VeinHub(QObject *qObjParent)
  : QObject(qObjParent),
    hubInitialized(false)
{
}

VeinPeer *VeinHub::getPeerByName(QString name)
{
  VeinPeer *retVal = 0;
  for(int peerCounter = 0; peerCounter<peerTable.count(); peerCounter++)
  {
    if(peerTable.values().at(peerCounter)->getName() == name)
    {
      retVal = peerTable.values().at(peerCounter);
      break;
    }
  }
  return retVal;
}

VeinPeer *VeinHub::getPeerByUID(const QUuid &peerUID)
{
  // 0 means default to null pointer if not found
  return peerTable.value(peerUID, 0);
}

bool VeinHub::initHub(protobuf::VeinProtocol *pMessage)
{
  bool retVal = false;
  if(!hubInitialized)
  {
    processInitHub(pMessage);
    retVal = true;
  }
  return retVal;
}

const QUuid &VeinHub::getUuid()
{
  return hubUuid;
}

bool VeinHub::setUuid(const QUuid &newUuid)
{
  bool retVal = false;
  if(!hubInitialized && hubUuid.isNull())
  {
    hubUuid = newUuid;
    retVal = true;
  }
  return retVal;
}

QList<VeinPeer *> VeinHub::listPeers()
{
  return peerTable.values();
}

VeinPeer *VeinHub::peerAdd(QString peerName)
{
  VeinPeer *vPeer = 0;
  if(!hubInitialized)
  {
    vPeer =new VeinPeer(peerName,this);
    if(Q_LIKELY(!peerTable.contains(vPeer->getPeerUuid())))
    {
      peerTable.insert(vPeer->getPeerUuid(),vPeer);
      sigPeerRegistered(vPeer);
    }
    else // It is impossible to generate the same QUuid twice within 1000 years, unless something is terribly wrong with the random number generator
    {
      Q_ASSERT(false);
      qCritical() << "[libvein-qt]" << __FILE__ << __LINE__ << "CRITICAL ERROR: There is a 2^121:1 chance that the random number generator of your system is contaminated";
    }
  }
  return vPeer;
}

void VeinHub::peerRemove(VeinPeer *vPeer)
{
  if(!hubInitialized && vPeer && peerTable.contains(vPeer->getPeerUuid()))
  {
    peerTable.remove(vPeer->getPeerUuid());
    vPeer->deleteLater();
  }
}

google::protobuf::Message *VeinHub::protobufHubInit()
{
  protobuf::VeinProtocol *retVal = new protobuf::VeinProtocol();
  protobuf::Vein_Command * cmd = retVal->mutable_command();
  QByteArray rfc4122BA;
  cmd->set_type(protobuf::Vein_Command::INIT_HUB);
  rfc4122BA = getUuid().toRfc4122();
  cmd->set_uidsender(rfc4122BA.data(), rfc4122BA.size());

  foreach(VeinPeer *tmpPeer, listPeers())
  {
    protobuf::Vein_Peer* protoPeer = retVal->add_peerlist();
    rfc4122BA = tmpPeer->getPeerUuid().toRfc4122();
    protoPeer->set_uid(rfc4122BA.data(), rfc4122BA.size());
    protoPeer->set_peername(tmpPeer->getName().toUtf8());
    for(int entityCount = 0; entityCount< tmpPeer->getEntityList().count(); entityCount++)
    {
      VeinEntity *tmpEnt = tmpPeer->getEntityList().at(entityCount);
      protobuf::Vein_Entity *protoEnt = protoPeer->add_peerentities();
      rfc4122BA = tmpEnt->getUuid().toRfc4122();
      protoEnt->set_uid(rfc4122BA.data(), rfc4122BA.size());
      rfc4122BA = tmpPeer->getPeerUuid().toRfc4122();
      protoEnt->set_owneruid(rfc4122BA.data(), rfc4122BA.size());
      protoEnt->set_entityname(tmpEnt->getName().toUtf8());
      for(int modCount=0; modCount<tmpEnt->getModifiers().count(); modCount++)
      {
        protoEnt->add_modifiers(tmpEnt->getModifiers().at(modCount));
      }
      protoEnt->set_datavariant(tmpEnt->getValue().toString().toUtf8());
      protoEnt->set_qmetatype(tmpEnt->getValue().type());
    }
  }
  return retVal;
}

google::protobuf::Message *VeinHub::protobufHubKill()
{
  protobuf::VeinProtocol *retVal = new protobuf::VeinProtocol();
  protobuf::Vein_Command * cmd = retVal->mutable_command();
  QByteArray rfc4122BA;
  cmd->set_type(protobuf::Vein_Command::KILL_HUB);
  rfc4122BA = getUuid().toRfc4122();
  cmd->set_uidsender(rfc4122BA.data(), rfc4122BA.size());
  cmd->add_uidreceiverlist(rfc4122BA.data(), rfc4122BA.size());

  return retVal;
}

google::protobuf::Message *VeinHub::protobufListPeers()
{
  /*
  protobuf::VeinProtocol *retVal = new protobuf::VeinProtocol();
  protobuf::Vein_Command * cmd = retVal->mutable_command();
  QByteArray rfc4122BA;
  //cmd->set_type(protobuf::Vein_Command::REQUEST_ANSWER);
  rfc4122BA = getUuid().toRfc4122();
  cmd->set_uidsender(rfc4122BA.data(), rfc4122BA.size());

  for(int peerCounter = 0; peerCounter<peerTable.count(); peerCounter++)
  {
    VeinPeer* tmpPeer = peerTable.values().at(peerCounter);
    protobuf::Vein_Peer* protoPeer = retVal->add_peerlist();
    rfc4122BA = tmpPeer->getPeerUuid().toRfc4122();
    protoPeer->set_uid(rfc4122BA.data(), rfc4122BA.size());
    protoPeer->set_peername(tmpPeer->getName().toUtf8());
    foreach(VeinEntity *tmpEnt, tmpPeer->getEntityList())
    {
      protobuf::Vein_Entity *protoEnt = protoPeer->add_peerentities();
      rfc4122BA = tmpEnt->getUuid().toRfc4122();
      protoEnt->set_uid(rfc4122BA.data(), rfc4122BA.size());
      rfc4122BA = vPeer->getPeerUuid().toRfc4122();
      protoEnt->set_owneruid(rfc4122BA.data(), rfc4122BA.size());
      protoEnt->set_entityname(tmpEnt->getName().toUtf8());
      for(int modCount=0; modCount<tmpEnt->getModifiers().count(); modCount++)
      {
        protoEnt->add_modifiers(tmpEnt->getModifiers().at(modCount));
      }
      protoEnt->set_datavariant(tmpEnt->getValue().toString().toUtf8());
      protoEnt->set_qmetatype(tmpEnt->getValue().type());
    }
  }
  return retVal;
  */
  return 0;
}

google::protobuf::Message *VeinHub::protobufListPeerEntities(VeinPeer *vPeer)
{
  protobuf::VeinProtocol *retVal = new protobuf::VeinProtocol();
  protobuf::Vein_Command * cmd = retVal->mutable_command();
  QByteArray rfc4122BA;
  cmd->set_type(protobuf::Vein_Command::REQUEST_PEER_DATA);
  rfc4122BA = getUuid().toRfc4122();
  cmd->set_uidsender(rfc4122BA.data(), rfc4122BA.size());

  protobuf::Vein_Peer* protoPeer = retVal->add_peerlist();
  rfc4122BA = vPeer->getPeerUuid().toRfc4122();
  protoPeer->set_uid(rfc4122BA.data(), rfc4122BA.size());
  protoPeer->set_peername(vPeer->getName().toUtf8());
  foreach(VeinEntity *tmpEnt, vPeer->getEntityList())
  {
    protobuf::Vein_Entity *protoEnt = protoPeer->add_peerentities();
    rfc4122BA = tmpEnt->getUuid().toRfc4122();
    protoEnt->set_uid(rfc4122BA.data(), rfc4122BA.size());
    rfc4122BA = vPeer->getPeerUuid().toRfc4122();
    protoEnt->set_owneruid(rfc4122BA.data(), rfc4122BA.size());
    protoEnt->set_entityname(tmpEnt->getName().toUtf8());
    for(int modCount=0; modCount<tmpEnt->getModifiers().count(); modCount++)
    {
      protoEnt->add_modifiers(tmpEnt->getModifiers().at(modCount));
    }
    protoEnt->set_datavariant(tmpEnt->getValue().toString().toUtf8());
    protoEnt->set_qmetatype(tmpEnt->getValue().type());
  }
  return retVal;
}

google::protobuf::Message *VeinHub::protobufAsyncEntityUpdate(VeinEntity *vEntity)
{
  //qDebug() << "MEMENTO";
  protobuf::VeinProtocol *retVal = 0;
  if(vEntity)
  {
    retVal = new protobuf::VeinProtocol();
    protobuf::Vein_Command * cmd = retVal->mutable_command();
    QByteArray rfc4122BA;
    cmd->set_type(protobuf::Vein_Command::ASYNC_ENTITY_UPDATE);
    rfc4122BA = getUuid().toRfc4122();
    cmd->set_uidsender(rfc4122BA.data(), rfc4122BA.size());
    cmd->add_uidreceiverlist(rfc4122BA.data(), rfc4122BA.size());

    protobuf::Vein_Entity *protoEnt = retVal->add_entitylist();
    rfc4122BA = vEntity->getUuid().toRfc4122();
    protoEnt->set_uid(rfc4122BA.data(), rfc4122BA.size());
    rfc4122BA = vEntity->getOwner()->getPeerUuid().toRfc4122();
    protoEnt->set_owneruid(rfc4122BA.data(), rfc4122BA.size());
    protoEnt->set_entityname(vEntity->getName().toUtf8());
    for(int modCount=0; modCount<vEntity->getModifiers().count(); modCount++)
    {
      protoEnt->add_modifiers(vEntity->getModifiers().at(modCount));
    }
    protoEnt->set_datavariant(vEntity->getValue().toByteArray().data(),vEntity->getValue().toByteArray().size());
    protoEnt->set_qmetatype(vEntity->getValue().type());
  }
  return retVal;
}

google::protobuf::Message *VeinHub::protobufRequestEntityData(VeinEntity *vEntity)
{
  protobuf::VeinProtocol *retVal = 0;
  if(vEntity)
  {
    retVal = new protobuf::VeinProtocol();
    protobuf::Vein_Command * cmd = retVal->mutable_command();
    QByteArray rfc4122BA;
    cmd->set_type(protobuf::Vein_Command::REQUEST_ENTITY_DATA);
    rfc4122BA = getUuid().toRfc4122();
    cmd->set_uidsender(rfc4122BA.data(), rfc4122BA.size());
    cmd->add_uidreceiverlist(rfc4122BA.data(), rfc4122BA.size());

    protobuf::Vein_Entity *protoEnt = retVal->add_entitylist();
    rfc4122BA = vEntity->getOwner()->getPeerUuid().toRfc4122();
    protoEnt->set_owneruid(rfc4122BA.data(), rfc4122BA.size());
    rfc4122BA = vEntity->getUuid().toRfc4122();
    protoEnt->set_uid(rfc4122BA.data(), rfc4122BA.size());
  }
  return retVal;
}

google::protobuf::Message *VeinHub::protobufRequestPeerData(VeinPeer *vPeer)
{
  /** @todo TBD */
  protobuf::VeinProtocol *retVal = 0;
  if(vPeer)
  {

  }
  return retVal;
}

google::protobuf::Message *VeinHub::protobufStartEntityAsyncUpdate(VeinEntity *vEntity)
{
  protobuf::VeinProtocol *retVal = 0;
  if(vEntity)
  {
    retVal = new protobuf::VeinProtocol();
    protobuf::Vein_Command * cmd = retVal->mutable_command();
    QByteArray rfc4122BA;
    cmd->set_type(protobuf::Vein_Command::REQUEST_ENTITY_START_ASYNC_UPDATE);
    //qDebug() << "protobufStartEntityAsyncUpdate:" <<getUuid();
    rfc4122BA = getUuid().toRfc4122();
    cmd->set_uidsender(rfc4122BA.data(), rfc4122BA.size());
    cmd->add_uidreceiverlist(rfc4122BA.data(), rfc4122BA.size());

    protobuf::Vein_Entity *protoEnt = retVal->add_entitylist();
    rfc4122BA = vEntity->getOwner()->getPeerUuid().toRfc4122();
    protoEnt->set_owneruid(rfc4122BA.data(), rfc4122BA.size());
    rfc4122BA = vEntity->getUuid().toRfc4122();
    protoEnt->set_uid(rfc4122BA.data(), rfc4122BA.size());
  }
  return retVal;
}

google::protobuf::Message *VeinHub::protobufStopEntityAsyncUpdate(VeinEntity *vEntity)
{
  protobuf::VeinProtocol *retVal = 0;
  if(vEntity)
  {
    retVal = new protobuf::VeinProtocol();
    protobuf::Vein_Command * cmd = retVal->mutable_command();
    QByteArray rfc4122BA;
    cmd->set_type(protobuf::Vein_Command::REQUEST_ENTITY_STOP_ASYNC_UPDATE);
    rfc4122BA = getUuid().toRfc4122();
    cmd->set_uidsender(rfc4122BA.data(), rfc4122BA.size());
    cmd->add_uidreceiverlist(rfc4122BA.data(), rfc4122BA.size());

    protobuf::Vein_Entity *protoEnt = retVal->add_entitylist();
    rfc4122BA = vEntity->getOwner()->getPeerUuid().toRfc4122();
    protoEnt->set_owneruid(rfc4122BA.data(), rfc4122BA.size());
    rfc4122BA = vEntity->getUuid().toRfc4122();
    protoEnt->set_uid(rfc4122BA.data(), rfc4122BA.size());
  }
  return retVal;
}

void VeinHub::onProtobufMessageReceived(google::protobuf::Message *pMessage)
{
  protobuf::VeinProtocol *proto = static_cast<protobuf::VeinProtocol *> (pMessage);

  if(proto)
  {
    //qDebug() << "## ## Message received:" << proto->command().type();
    switch(proto->command().type())
    {
      case protobuf::Vein_Command::ASYNC_ENTITY_UPDATE:
      {
        processAsyncEntityUpdate(proto);
        break;
      }
      case protobuf::Vein_Command::ASYNC_PEERLIST_UPDATE:
      {
        processAsyncPeerlistUpdate(proto);
        break;
      }
      case protobuf::Vein_Command::ASYNC_PEER_UPDATE:
      {
        processAsyncPeerUpdate(proto);
        break;
      }
      /* // all answers will be sent as async_<...>_update
      case protobuf::Vein_Command::REQUEST_ANSWER:
      {
        break;
      }
      */
      case protobuf::Vein_Command::REQUEST_ENTITY_DATA:
      {
        processRequestEntityData(proto);
        break;
      }
      case protobuf::Vein_Command::REQUEST_ENTITY_START_ASYNC_UPDATE:
      {
        processRequestEntityStartAsyncUpdate(proto);
        break;
      }
      case protobuf::Vein_Command::REQUEST_ENTITY_STOP_ASYNC_UPDATE:
      {
        processRequestEntityStopAsyncUpdate(proto);
        break;
      }
      case protobuf::Vein_Command::REQUEST_PEERLIST:
      {
        processRequestPeerlist(proto);
        break;
      }
      case protobuf::Vein_Command::REQUEST_PEER_DATA:
      {
        processRequestPeerData(proto);
        break;
      }
      case protobuf::Vein_Command::MESSAGE_DEBUG:
      {
        processMessageDebug(proto);
        break;
      }
      case protobuf::Vein_Command::MESSAGE_ERROR:
      {
        processMessageError(proto);
        break;
      }
      case protobuf::Vein_Command::INIT_HUB:
      {
        processInitHub(proto);
        break;
      }
      case protobuf::Vein_Command::KILL_HUB:
      {
        processKillHub(proto);
        break;
      }
      default:
      {
        qWarning() << "[libvein-qt]" << __FILE__ << __LINE__ << "Unhandled protobuf messagetype: " <<proto->command().type();
        break;
      }
    }
  }
  /** @todo TBD */
}

void VeinHub::processAsyncEntityUpdate(protobuf::VeinProtocol *pMessage)
{
  if(pMessage)
  {
    //expected is exactly 1 entity at position 0
    if(pMessage->entitylist_size()==1)
    {
      protobuf::Vein_Entity protoEnt = pMessage->entitylist(0);
      VeinPeer *tmpPeer = getPeerByUID(QUuid::fromRfc4122(QByteArray(protoEnt.owneruid().c_str(),protoEnt.owneruid().size())));
      if(tmpPeer)
      {
        VeinEntity *tmpEntity = tmpPeer->getEntityByUuid(QUuid::fromRfc4122(QByteArray(protoEnt.uid().c_str(),protoEnt.uid().size())));
        if(tmpEntity)
        {
          QString tmpDataString = QString::fromUtf8(protoEnt.datavariant().c_str(),protoEnt.datavariant().size());
          QVariant tmpData = tmpDataString;
          //QVariant::Type tmpType = static_cast<QVariant::Type>(protoEnt.qmetatype());
          QList<VeinEntity::Modifiers> tmpModifierList;
          for(int i=0; i<protoEnt.modifiers_size(); i++)
          {
            VeinEntity::Modifiers tmpMods = static_cast<VeinEntity::Modifiers>(protoEnt.modifiers(i));
            tmpModifierList.append(tmpMods);
          }

          tmpEntity->setValueExtern(tmpData);
          //tmpEntity->onRemoteValueChanged(tmpData);
          tmpEntity->onRemoteModifersChanged(tmpModifierList);
          //qDebug()<<"processAsyncEntityUpdate";
        }
      }
    }
  }
}

void VeinHub::processAsyncPeerlistUpdate(protobuf::VeinProtocol *pMessage)
{
  /*
  for(int peerCounter = 0; peerCounter<pMessage->peerlist_size(); peerCounter++)
  {
    QUuid tmpUuid = QUuid::fromRfc4122(QByteArray(pMessage->peerlist(peerCounter).uid().c_str(),pMessage->peerlist(i).uid().size()));
    VeinPeer* tmpPeer = getPeerByUID(tmpUuid);
    if(tmpPeer)
    {
      QString tmpName = QString::fromUtf8(pMessage->peerlist(peerCounter).peername().c_str());
      if(tmpPeer->getName()!=tmpName)
      {
        tmpPeer->setName(tmpName);
      }
    }
    else //A new peer was added
    {
      protobuf::VeinProtocol newProtocol;

      protobuf::Vein_Command *cmd = newProtocol.mutable_command();
      cmd->set_type(protobuf::Vein_Command::INIT_PEER);

      protobuf::Vein_Peer *pPeer = newProtocol.add_peerlist();
      //overwrite the peer with the actual data
      *pPeer = protobuf::Vein_Peer(pMessage->peerlist(peerCounter));

      VeinPeer *newPeer = new VeinPeer(tmpUuid, this, &newProtocol);
      peerTable.insert(tmpUuid, newPeer);
      sigPeerRegistered(newPeer);
    }
  }
  return retVal;
  */
}

void VeinHub::processAsyncPeerUpdate(protobuf::VeinProtocol *pMessage)
{
  /** @todo TBD */
}

void VeinHub::processRequestEntityData(protobuf::VeinProtocol *pMessage)
{
  if(pMessage)
  {
    //expected is exactly 1 entity at position 0
    if(pMessage->entitylist_size()==1)
    {
      protobuf::Vein_Entity protoEnt = pMessage->entitylist(0);
      VeinPeer *tmpPeer = getPeerByUID(QUuid::fromRfc4122(QByteArray(protoEnt.owneruid().c_str(),protoEnt.owneruid().size())));
      if(tmpPeer)
      {
        VeinEntity *tmpEntity = tmpPeer->getEntityByUuid(QUuid::fromRfc4122(QByteArray(protoEnt.uid().c_str(),protoEnt.uid().size())));
        if(tmpEntity)
        {
          google::protobuf::Message *tmpMessage = protobufAsyncEntityUpdate(tmpEntity);
          sigSendProtobufMessage(tmpMessage);
          delete tmpMessage;
        }
      }
    }
  }
}

void VeinHub::processRequestEntityStartAsyncUpdate(protobuf::VeinProtocol *pMessage)
{
  //expected is exactly 1 entity at position 0
  if(pMessage->entitylist_size()==1)
  {
    //qDebug() << "I14 HUBUUID:" << this->hubUuid;
    protobuf::Vein_Entity protoEnt = pMessage->entitylist(0);
    QUuid tmpUuid = QUuid::fromRfc4122(QByteArray(protoEnt.owneruid().c_str(),protoEnt.owneruid().size()));
    VeinPeer *tmpPeer = getPeerByUID(tmpUuid);
    if(tmpPeer)
    {
      VeinEntity *tmpEntity = tmpPeer->getEntityByUuid(QUuid::fromRfc4122(QByteArray(protoEnt.uid().c_str(),protoEnt.uid().size())));
      if(tmpEntity)
      {
        //qDebug() << "I10 processRequestEntityStartAsyncUpdate  hub.uuid:" << QUuid::fromRfc4122(QByteArray(pMessage->command().uidsender().c_str(),pMessage->command().uidsender().size()));
        tmpEntity->onRemoteConnectionAdded(QUuid::fromRfc4122(QByteArray(pMessage->command().uidsender().c_str(),pMessage->command().uidsender().size())));
      }
    }
    else
    {
      qDebug() << hubUuid << "E3 peer not found:" << tmpUuid << "\npeers:" << peerTable;
    }
  }
}

void VeinHub::processRequestEntityStopAsyncUpdate(protobuf::VeinProtocol *pMessage)
{
  //expected is exactly 1 entity at position 0
  if(pMessage->entitylist_size()==1)
  {
    protobuf::Vein_Entity protoEnt = pMessage->entitylist(0);
    VeinPeer *tmpPeer = getPeerByUID(QUuid::fromRfc4122(QByteArray(protoEnt.owneruid().c_str(),protoEnt.owneruid().size())));

    if(tmpPeer)
    {
      VeinEntity *tmpEntity = tmpPeer->getEntityByUuid(QUuid::fromRfc4122(QByteArray(protoEnt.uid().c_str(),protoEnt.uid().size())));
      if(tmpEntity)
      {
        tmpEntity->onRemoteConnectionRemoved(QUuid::fromRfc4122(QByteArray(pMessage->command().uidsender().c_str(),pMessage->command().uidsender().size())));
      }
    }
  }
}

void VeinHub::processRequestPeerlist(protobuf::VeinProtocol *pMessage)
{
  Q_UNUSED(pMessage);
  protobuf::VeinProtocol retVal;
  protobuf::Vein_Command * cmd = retVal.mutable_command();
  QByteArray rfc4122BA;
  cmd->set_type(protobuf::Vein_Command::ASYNC_PEERLIST_UPDATE);
  rfc4122BA = getUuid().toRfc4122();
  cmd->set_uidsender(rfc4122BA.data(), rfc4122BA.size());

  foreach(VeinPeer *tmpPeer, listPeers())
  {
    protobuf::Vein_Peer* protoPeer = retVal.add_peerlist();
    rfc4122BA = tmpPeer->getPeerUuid().toRfc4122();
    protoPeer->set_uid(rfc4122BA.data(), rfc4122BA.size());
    protoPeer->set_peername(tmpPeer->getName().toUtf8());
    for(int entityCount = 0; entityCount< tmpPeer->getEntityList().count(); entityCount++)
    {
      VeinEntity *tmpEnt = tmpPeer->getEntityList().at(entityCount);
      protobuf::Vein_Entity *protoEnt = protoPeer->add_peerentities();
      rfc4122BA = tmpEnt->getUuid().toRfc4122();
      protoEnt->set_uid(rfc4122BA.data(), rfc4122BA.size());
      rfc4122BA = tmpPeer->getPeerUuid().toRfc4122();
      protoEnt->set_owneruid(rfc4122BA.data(), rfc4122BA.size());
      protoEnt->set_entityname(tmpEnt->getName().toUtf8());
      for(int modCount=0; modCount<tmpEnt->getModifiers().count(); modCount++)
      {
        protoEnt->add_modifiers(tmpEnt->getModifiers().at(modCount));
      }
      protoEnt->set_datavariant(tmpEnt->getValue().toString().toUtf8());
      protoEnt->set_qmetatype(tmpEnt->getValue().type());
    }
  }
  sigSendProtobufMessage(&retVal);
}

void VeinHub::processRequestPeerData(protobuf::VeinProtocol *pMessage)
{
  if(pMessage->peerlist_size()==1)
  {
    //qDebug() << "I14 HUBUUID:" << this->hubUuid;
    protobuf::Vein_Peer aPeer = pMessage->peerlist(0);
    QUuid tmpUuid = QUuid::fromRfc4122(QByteArray(aPeer.uid().c_str(),aPeer.uid().size()));
    VeinPeer *tmpPeer = getPeerByUID(tmpUuid);
    if(tmpPeer)
    {
      protobuf::VeinProtocol retVal;
      protobuf::Vein_Command * cmd = retVal.mutable_command();
      QByteArray rfc4122BA;
      cmd->set_type(protobuf::Vein_Command::ASYNC_PEER_UPDATE);
      rfc4122BA = getUuid().toRfc4122();
      cmd->set_uidsender(rfc4122BA.data(), rfc4122BA.size());
      protobuf::Vein_Peer* protoPeer = retVal.add_peerlist();
      rfc4122BA = tmpPeer->getPeerUuid().toRfc4122();
      protoPeer->set_uid(rfc4122BA.data(), rfc4122BA.size());
      protoPeer->set_peername(tmpPeer->getName().toUtf8());
      for(int entityCount = 0; entityCount< tmpPeer->getEntityList().count(); entityCount++)
      {
        VeinEntity *tmpEnt = tmpPeer->getEntityList().at(entityCount);
        protobuf::Vein_Entity *protoEnt = protoPeer->add_peerentities();
        rfc4122BA = tmpEnt->getUuid().toRfc4122();
        protoEnt->set_uid(rfc4122BA.data(), rfc4122BA.size());
        rfc4122BA = tmpPeer->getPeerUuid().toRfc4122();
        protoEnt->set_owneruid(rfc4122BA.data(), rfc4122BA.size());
        protoEnt->set_entityname(tmpEnt->getName().toUtf8());
        for(int modCount=0; modCount<tmpEnt->getModifiers().count(); modCount++)
        {
          protoEnt->add_modifiers(tmpEnt->getModifiers().at(modCount));
        }
        protoEnt->set_datavariant(tmpEnt->getValue().toString().toUtf8());
        protoEnt->set_qmetatype(tmpEnt->getValue().type());
      }
      sigSendProtobufMessage(&retVal);
    }
  }
}

void VeinHub::processMessageDebug(protobuf::VeinProtocol *pMessage)
{
  /** @todo TBD for now just show the simplest debug message */
  qDebug() << pMessage->DebugString().c_str();
}

void VeinHub::processMessageError(protobuf::VeinProtocol *pMessage)
{
  /** @todo TBD for now just show the simplest debug message */
  qCritical() << pMessage->DebugString().c_str();
}

void VeinHub::processInitHub(protobuf::VeinProtocol *pMessage)
{
  if(hubInitialized==false)
  {
    if(pMessage && pMessage->has_command())
    {
      protobuf::Vein_Command cmd = pMessage->command();
      if(cmd.type()==protobuf::Vein_Command::INIT_HUB)
      {
        hubUuid = QUuid::fromRfc4122(QByteArray(cmd.uidsender().c_str(),cmd.uidsender().size()));
        //qDebug() << "I6 Hub Uuid set:" << hubUuid;
        for(int i=0; i<pMessage->peerlist_size(); i++)
        {
          protobuf::VeinProtocol newProtocol;
          protobuf::Vein_Command *peerCommand = newProtocol.mutable_command();
          protobuf::Vein_Peer *tmpPeer = newProtocol.add_peerlist();
          peerCommand->set_type(protobuf::Vein_Command::INIT_PEER);
          // copy the Vein_Peer from the other protocol buffer


          *tmpPeer = protobuf::Vein_Peer(pMessage->peerlist(i));
          QUuid tmpUuid = QUuid::fromRfc4122(QByteArray(tmpPeer->uid().c_str(),tmpPeer->uid().size()));
          //qDebug() << "I4 Creating peer with QUuid:" << tmpUuid;
          if(tmpUuid.isNull())
          {
            qDebug() << tmpUuid << pMessage->DebugString().c_str();
          }
          VeinPeer *newPeer = new VeinPeer(tmpUuid, this, &newProtocol);
          peerTable.insert(tmpUuid, newPeer);
          sigPeerRegistered(newPeer);
        }

        hubInitialized = true;
      }
    }
  }
}

void VeinHub::processKillHub(protobuf::VeinProtocol *pMessage)
{
  Q_UNUSED(pMessage);
  /** @todo TBD */
}
