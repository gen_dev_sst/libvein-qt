#include "veinpeer.h"
#include "veinentity.h"
#include "veinhub.h"

#include <veincore.pb.h>

VeinPeer::VeinPeer(QString name, VeinHub *qObjParent) :
  QObject(qObjParent),
  peerName(name),
  peerRemote(false),
  peerUuid(QUuid::createUuid())
{
  hub = qObjParent;
}

VeinPeer::VeinPeer(const QUuid &uuid, VeinHub *qObjParent, google::protobuf::Message *pMessage)
  : QObject(qObjParent),
    peerRemote(true),
    peerUuid(uuid)
{
  hub = qObjParent;
  protobuf::VeinProtocol *proto = static_cast<protobuf::VeinProtocol *>(pMessage);
  if(proto)
  {
    if(proto->command().type()==protobuf::Vein_Command::INIT_PEER)
    {
      //we expect only one Vein_Peer at position 0
      if(proto->peerlist_size()==1)
      {
        protobuf::Vein_Peer tmpPeer = proto->peerlist(0);
        // check stupid mistakes made by tired programmers
        if(tmpPeer.has_peername())
        {
          peerName = QString::fromUtf8(tmpPeer.peername().c_str());
          for(int i = 0; i<tmpPeer.peerentities_size(); i++)
          {
            protobuf::VeinProtocol tmpProtocol;
            tmpProtocol.mutable_command()->set_type(protobuf::Vein_Command::INIT_ENTITY);
            /** @todo test if copy constructor of Vein_Entity works as expected */
            // copy the entity over
            protobuf::Vein_Entity *tmpEnt = tmpProtocol.add_entitylist();
            *tmpEnt = protobuf::Vein_Entity(tmpPeer.peerentities(i));

            QUuid entUuid = QUuid::fromRfc4122(QByteArray(tmpEnt->uid().c_str(),tmpEnt->uid().size()));
            VeinEntity * newEntity = new VeinEntity(entUuid, this, &tmpProtocol);
            dataPointers.insert(entUuid, newEntity);
          }
        }
      }
    }
  }
}

VeinEntity *VeinPeer::getEntityByUuid(const QUuid &uuid)
{
  return dataPointers.value(uuid,0);
}

VeinEntity *VeinPeer::getEntityByName(QString name)
{
  VeinEntity * retVal = 0;
  foreach(VeinEntity *tmpEnt, dataPointers.values())
  {
    if(tmpEnt->getName() == name)
    {
      retVal = tmpEnt;
      break;
    }
  }
  return retVal;
}

QList<VeinEntity *> VeinPeer::getEntityList()
{
  return dataPointers.values();
}

VeinHub *VeinPeer::getHub()
{
  return hub;
}

QString VeinPeer::getName()
{
  return peerName;
}

void VeinPeer::setName(QString newName)
{
  if(!newName.isNull())
  {
    peerName = newName;
  }
}

const QUuid &VeinPeer::getPeerUuid()
{
  return peerUuid;
}

bool VeinPeer::isRemote()
{
  return peerRemote;
}

void VeinPeer::fetchPeerData()
{
  /** @todo TBD */
}

VeinEntity *VeinPeer::dataAdd(QString name)
{
  VeinEntity *retVal = 0;
  retVal = new VeinEntity(this,name);
  dataPointers.insert(retVal->getUuid(), retVal);
  sigIntrospectionUpdated();
  return retVal;
}

void VeinPeer::dataRemove(VeinEntity *data)
{
  if(data && dataPointers.contains(data->getUuid()))
  {
    dataPointers.remove(data->getUuid());
    data->deleteLater();
    sigIntrospectionUpdated();
  }
}


