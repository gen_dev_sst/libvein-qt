#include "veinentity.h"

#include "veinpeer.h"
#include "veinhub.h"

#include <veincore.pb.h>

#include <QMetaMethod>
#include <QByteArray>

#include <QDebug>



VeinEntity::VeinEntity(VeinPeer *peer, QString name) :
  QObject(peer),
  entityName(name),
  entityUuid(QUuid::createUuid()),
  owner(peer),
  connectionCount(0)
{
  setupSigSlots();
}


VeinEntity::VeinEntity(const QUuid &uuid, VeinPeer *peer, google::protobuf::Message *pMessage)
  : QObject(peer),
    entityUuid(uuid),
    owner(peer),
    connectionCount(0)
{
  setupSigSlots();
  protobuf::VeinProtocol *proto = static_cast<protobuf::VeinProtocol *>(pMessage);
  if(proto)
  {
    if(proto->command().type()==protobuf::Vein_Command::INIT_ENTITY)
    {
      // we expect exactly 1 entity at position 0
      if(proto->entitylist_size()==1)
      {
        protobuf::Vein_Entity tmpEnt = proto->entitylist(0);

        //check for stupid mistakes of tired programmers
        if(tmpEnt.has_datavariant() && tmpEnt.has_entityname() && tmpEnt.has_qmetatype())
        {
          entityName = QString::fromUtf8(tmpEnt.entityname().c_str());
          //qDebug() << "Entity created:" << entityName;
          QList<Modifiers> tmpMods;
          for(int i=0; i<tmpEnt.modifiers_size(); i++)
          {
            Modifiers m = static_cast<Modifiers>(tmpEnt.modifiers(i));
            tmpMods.append(m);
          }
          entityModifiers = tmpMods;

          QString tmpData = QString::fromUtf8(tmpEnt.datavariant().c_str(),tmpEnt.datavariant().size());
          data = tmpData;

          connect(this, &VeinEntity::sigProtobufModifiersChanged, getHub(), &VeinHub::sigSendProtobufMessage);
          connect(this, &VeinEntity::sigProtobufValueChanged, getHub(), &VeinHub::sigSendProtobufMessage);
        }
      }
    }
  }
}

VeinHub *VeinEntity::getHub()
{
  return owner->getHub();
}

QList<VeinEntity::Modifiers> VeinEntity::getModifiers()
{
  return entityModifiers;
}

QString VeinEntity::getName()
{
  return entityName;
}

VeinPeer *VeinEntity::getOwner()
{
  return owner;
}

QList<QUuid> VeinEntity::getRemoteConnections()
{
  return remoteConnections;
}

const QUuid &VeinEntity::getUuid()
{
  return entityUuid;
}

QVariant VeinEntity::getValue()
{
  /** @todo TBD add code for fetching data from foreign peers */
  return data;
}



void VeinEntity::setValue(QVariant newData, VeinPeer *issuer)
{
  /// @note calls with invalid issuer
  this->setRealValue(newData,issuer);
}

void VeinEntity::setValueExtern(QVariant newData)
{
  // do not compare floating point values with ==
  if(data.type()==QVariant::Double
     && newData.type()==QVariant::Double)
  {
    if(!qFuzzyCompare(data.toDouble(),newData.toDouble())
       || !entityModifiers.contains(MOD_NOECHO))
    {
      data = newData;
      //VeinDebugGpio::getInstance()->trigger();
      sigValueChanged(newData);
    }
  }
  else if(data!=newData
          || !entityModifiers.contains(MOD_NOECHO))
  {
    data = newData;
    //VeinDebugGpio::getInstance()->trigger();
    sigValueChanged(newData);
  }
}

void VeinEntity::fetchEntityData()
{
  owner->getHub()->sigSendProtobufMessage(owner->getHub()->protobufRequestEntityData(this));
}

bool VeinEntity::modifiersAdd(VeinEntity::Modifiers mod)
{
  bool retVal = false;
  if(!entityModifiers.contains(mod))
  {
    entityModifiers.append(mod);
    sigModifiersChanged(entityModifiers);
    sigProtobufModifiersChanged(owner->getHub()->protobufAsyncEntityUpdate(this));
    retVal = true;
  }
  return retVal;
}

bool VeinEntity::modifiersClear()
{
  bool retVal = false;

  entityModifiers.clear();
  retVal = true;
  return retVal;
}

bool VeinEntity::modifiersRemove(VeinEntity::Modifiers mod)
{
  bool retVal = false;
  if(entityModifiers.contains(mod))
  {
    entityModifiers.removeAll(mod);
    sigModifiersChanged(entityModifiers);
    sigProtobufModifiersChanged(owner->getHub()->protobufAsyncEntityUpdate(this));
    retVal = true;
  }
  return retVal;
}

void VeinEntity::onAsyncUpdateStarted()
{
  //qDebug() << "connectNotify";
  google::protobuf::Message *tmpMessage =getHub()->protobufStartEntityAsyncUpdate(this);
  getHub()->sigSendProtobufMessage(tmpMessage);
  delete tmpMessage;
}

void VeinEntity::onAsyncUpdateStopped()
{
  //qDebug() << "disconnectNotify";
  google::protobuf::Message *tmpMessage = getHub()->protobufStopEntityAsyncUpdate(this);
  getHub()->sigSendProtobufMessage(tmpMessage);
  delete tmpMessage;
}

void VeinEntity::onRemoteConnectionAdded(const QUuid &rConnection)
{
  if(!remoteConnections.contains(rConnection))
  {
    remoteConnections.append(rConnection);
  }
  if(!remoteConnections.isEmpty())
  {
    connect(this, &VeinEntity::sigProtobufModifiersChanged, getHub(), &VeinHub::sigSendProtobufMessage);
    connect(this, &VeinEntity::sigProtobufValueChanged, getHub(), &VeinHub::sigSendProtobufMessage);
  }
}

void VeinEntity::onRemoteConnectionRemoved(const QUuid &rConnection)
{
  if(remoteConnections.contains(rConnection))
  {
    remoteConnections.removeAll(rConnection);
  }
  if(remoteConnections.isEmpty())
  {
    disconnect(this, &VeinEntity::sigProtobufModifiersChanged, getHub(), &VeinHub::sigSendProtobufMessage);
    disconnect(this, &VeinEntity::sigProtobufValueChanged, getHub(), &VeinHub::sigSendProtobufMessage);
  }
}

void VeinEntity::onRemoteModifersChanged(QList<VeinEntity::Modifiers> mod)
{
  entityModifiers = mod;
}

void VeinEntity::onRemoteValueChanged(QVariant newData)
{
  // do not compare floating point values with ==
  if(data.type()==QVariant::Double
     && newData.type()==QVariant::Double)
  {
    if(!qFuzzyCompare(data.toDouble(),newData.toDouble())
       || !entityModifiers.contains(MOD_NOECHO))
    {
      data = newData;
      sigValueChanged(newData);
    }
  }
  else if(data!=newData
          || !entityModifiers.contains(MOD_NOECHO))
  {
    data = newData;
    sigValueChanged(newData);
  }
}

void VeinEntity::setupSigSlots()
{
  connect(this,&VeinEntity::sigAsyncUpdatesStart,this,&VeinEntity::onAsyncUpdateStarted);
  connect(this,&VeinEntity::sigAsyncUpdatesStop,this,&VeinEntity::onAsyncUpdateStopped);
}

void VeinEntity::setRealValue(QVariant newData, VeinPeer *issuer)
{
  bool pv_updateValue = false;
  if(!newData.isNull())
  {
    if(!entityModifiers.contains(MOD_CONST))
    {
      if(!entityModifiers.contains(MOD_READONLY)
         || issuer==owner)
      {
        // do not compare floating point values with ==
        if(data.type()==QVariant::Double
           && newData.type()==QVariant::Double)
        {
          if(!entityModifiers.contains(MOD_NOECHO)
             || !qFuzzyCompare(data.toDouble(),newData.toDouble()))
          {
            pv_updateValue = true;
          }
        }
        else if(!entityModifiers.contains(MOD_NOECHO)
                || data!=newData)
        {
          pv_updateValue = true;
        }
      }
      else
      {
        qWarning() << "[libvein] Failed to overwrite the value of an Entity that is flagged with MOD_READONLY";
      }
    }
    else
    {
      qWarning() << "[libvein] Failed to overwrite the value of an Entity that is flagged with MOD_CONST";
    }
  }
  else
  {
    // empty value for event transmission
    pv_updateValue = true;
  }
  if(pv_updateValue==true)
  {
    data = newData;
    sigValueChanged(newData);
    //VeinDebugGpio::getInstance()->trigger();
    google::protobuf::Message *tmpMessage = getHub()->protobufAsyncEntityUpdate(this);
    sigProtobufValueChanged(tmpMessage);
    delete tmpMessage;
  }
}

void VeinEntity::connectNotify(const QMetaMethod &qmmSignal)
{
  QObject::connectNotify(qmmSignal);
  if(qmmSignal == QMetaMethod::fromSignal(&VeinEntity::sigValueChanged)
     || qmmSignal == QMetaMethod::fromSignal(&VeinEntity::sigModifiersChanged))
  {
    // no one was connected previously
    if(connectionCount==0)
    {
      sigAsyncUpdatesStart();
    }
    connectionCount++;
    //qDebug() << "connectNotify" << this << connectionCount;
  }
}

void VeinEntity::disconnectNotify(const QMetaMethod &qmmSignal)
{
  QObject::disconnectNotify(qmmSignal);
  if(qmmSignal == QMetaMethod::fromSignal(&VeinEntity::sigValueChanged)
     || qmmSignal == QMetaMethod::fromSignal(&VeinEntity::sigModifiersChanged))
  {
    connectionCount--;
    // no one is connected anymore
    if(connectionCount==0)
    {
      sigAsyncUpdatesStop();
    }
    // qDebug() << "disconnectNotify" << this << connectionCount;
  }
}




