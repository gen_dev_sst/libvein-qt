#ifndef VEINPEER_H
#define VEINPEER_H

#include "libveinqt_global.h"

#include <QHash>
#include <QObject>
#include <QUuid>

QT_BEGIN_NAMESPACE
class QString;
QT_END_NAMESPACE


class VeinAuth;
class VeinEntity;
class VeinHub;
class VeinPeer;

namespace google
{
  namespace protobuf
  {
    class Message;
  }
}

/**
 * @brief Network abstraction for clients
 */
class LIBVEINQTSHARED_EXPORT VeinPeer : public QObject
{
  Q_OBJECT
public:


  /**
   * @brief VeinPeer default constructor
   * @param name
   * @param[in] qObjParent Parent hub
   */
  explicit VeinPeer(QString name, VeinHub *qObjParent=0);

  /**
   * @brief VeinPeer Constructor to be used when created from protobuf
   * @param[in] uuid
   * @param[in] qObjParent Parent hub
   * @param[in] pMessage
   */
  VeinPeer(const QUuid &uuid, VeinHub *qObjParent, google::protobuf::Message *pMessage);

  /**
   * @brief A fast lookup in the hashtable to obtain the entity
   * @param[in] uuid
   * @return 0 pointer in case of failure
   */
  VeinEntity *getEntityByUuid(const QUuid &uuid);

  /**
   * @brief A slow but convenient way to acess a VeinEntity
   * @param name
   * @return 0 pointer in case of failure
   */
  VeinEntity *getEntityByName(QString name);

  /**
   * @brief A simple QList of the entities for iterations
   * @return
   */
  QList<VeinEntity *> getEntityList();


  /**
   * @brief Returns the parent
   * @return
   */
  VeinHub *getHub();

  /**
   * @brief The name may be ambiguous
   * @return
   */
  QString getName();
  void setName(QString newName);

  /**
   * @brief getPeerUID
   * @return
   */
  const QUuid &getPeerUuid();

  /**
   * @brief isRemote
   * @return
   * @todo This is maybe useless?
   */
  bool isRemote();


signals:
  /**
   * @brief Indicates that the introspection changed
   */
  void sigIntrospectionUpdated();

public slots:

  /**
   * @brief Fetches all data for this VeinPeer from the remote VeinHub, updates all child VeinEntity instances
   */
  void fetchPeerData();

  /**
   * @brief Constructs a local VeinEntity and exposes it in the introspection
   * @param name Has to be unique in this VeinPeer
   * @return 0 for failure
   */
  VeinEntity *dataAdd(QString name);

  /**
   * @brief Cancels introspection for data
   * @param[in] data
   */
  void dataRemove(VeinEntity *data);

private:
  //pimpl:
  /**
   * @brief Authentification token
   * @todo PIMPL
   */
  VeinAuth *auth;

  /**
   * @brief dataPointers
   * @todo PIMPL
   */
  QHash<QUuid, VeinEntity *> dataPointers;

  /**
   * @brief Name of this peer
   * @todo PIMPL
   */
  QString peerName;

  VeinHub *hub;

  /**
   * @brief Distinguishes between remote and local peers (otherwise a RTTI check would be required)
   */
  const bool peerRemote;

  /**
   * @brief peerUuid
   */
  const QUuid peerUuid;


  Q_DISABLE_COPY(VeinPeer)
};

#endif // VEINPEER_H
