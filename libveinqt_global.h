#ifndef LIBVEIN_GLOBAL_H
#define LIBVEIN_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(VEINQT_LIBRARY)
#  define LIBVEINQTSHARED_EXPORT Q_DECL_EXPORT
#else
#  define LIBVEINQTSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // LIBVEIN_GLOBAL_H
