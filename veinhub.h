#ifndef VEINMANAGER_H
#define VEINMANAGER_H

#include "libveinqt_global.h"


#include <QObject>
#include <QList>
#include <QHash>
#include <QUuid>


class VeinEntity;
class VeinPeer;

namespace protobuf
{
  class VeinProtocol;
}

namespace google
{
  namespace protobuf
  {
    class Message;
  }
}

class LIBVEINQTSHARED_EXPORT VeinHub : public QObject
{
  Q_OBJECT
public:

  /**
   * @brief Private constructor, singleton
   */
  VeinHub(QObject *qObjParent=0);

  /**
   * @brief A slow but conveniant way to access VeinPeer instances
   * @param name
   * @return 0 if not found, otherwise a pointer to the VeinPeer is returned
   */
  VeinPeer *getPeerByName(QString name);

  /**
   * @brief A fast lookup in the hash table
   * @param[in] peerUID
   * @return 0 if not found
   */
  VeinPeer *getPeerByUID(const QUuid &peerUID);

  bool initHub(protobuf::VeinProtocol *pMessage);

  /**
   * @brief getUuid
   * @return QUuid of this hub is returned (may be uninitialized)
   */
  const QUuid &getUuid();

  /**
   * @brief setUuid
   * @param[in] newUuid
   * @return true for success
   * @note Will fail if the QUuid was already set previously
   */
  bool setUuid(const QUuid &newUuid);

  /**
   * @brief Lists all peers depending on their affiliation
   *
   * Defaults to both local and foreign
   * @return List of known VeinPeer instances
   */
  QList<VeinPeer *> listPeers();

  /**
   * @brief Creates a new peer
   * @param peerName This name should be unique (not checking for duplicates)
   * @return 0 ptr on error
   */
  VeinPeer *peerAdd(QString peerName);

  /**
   * @brief Removes the peer from the list and calls deleteLater()
   * @param[in] vPeer
   */
  void peerRemove(VeinPeer *vPeer);

  /**
   * @brief Generates initializing code
   * @return
   */
    google::protobuf::Message *protobufHubInit();
  /**
   * @brief Generates a protobuf message that tells the receiver that this Hub will log off
   * @return
   */
  google::protobuf::Message *protobufHubKill();
  /**
   * @brief Generates a protobuf encapsulated list of VeinPeer (without VeinEntity info)
   * @return
   */
  google::protobuf::Message *protobufListPeers();
  /**
   * @brief Generates a protobuf encapsulated list of VeinEntity instances of vPeer
   * @param[in] vPeer
   * @return
   */
  google::protobuf::Message *protobufListPeerEntities(VeinPeer *vPeer);
  /**
   * @brief Generates a protobuf encapsulated Memento of vEntity
   * @param[in] vEntity
   * @return 0 pointer on error
   * @todo Rename function as the name sounds odd
   */
  google::protobuf::Message *protobufAsyncEntityUpdate(VeinEntity *vEntity);
  /**
   * @brief Generates a protobuf request for the data of vEntity
   * @param[in] vEntity
   * @return 0 pointer on error
   * @note The answer will be asynchronous, because it is not defined wether other messages will be transmitted between request and answer
   */
  google::protobuf::Message *protobufRequestEntityData(VeinEntity *vEntity);
  /**
   * @brief Generates a protobuf request for the data of vPeer
   * @param vPeer
   * @return 0 pointer on error
   * @note The answer will be asynchronous, because it is not defined wether other messages will be transmitted between request and answer
   */
  google::protobuf::Message *protobufRequestPeerData(VeinPeer *vPeer);
  /**
   * @brief Generates a protobuf request to send asynchronous updates for vEntity
   * @param[in] vEntity
   * @return 0 pointer on error
   */
  google::protobuf::Message *protobufStartEntityAsyncUpdate(VeinEntity *vEntity);
  /**
   * @brief Generates a protobuf request to stop sending asynchronous updates for vEntity
   * @param[in] vEntity
   * @return 0 pointer on error
   */
  google::protobuf::Message *protobufStopEntityAsyncUpdate(VeinEntity *vEntity);

signals:
  /**
   * @brief Will be called for every peer added, or created from init code
   * @param[in] peer
   */
  void sigPeerRegistered(VeinPeer *peer);
  /**
   * @brief Notifies a higher layer that there is a message to send
   * @param[in] message
   */
  void sigSendProtobufMessage(google::protobuf::Message *message);

public slots:
  /**
   * @brief The opposite of sigSendProtobufMessage
   *
   * will be called when messages arrive that are addressed to this hub
   * @param[in] pMessage
   */
  void onProtobufMessageReceived(google::protobuf::Message *pMessage);


private:
  // PIMPL
  /**
   * @brief List of active peers
   */
  QHash<QUuid, VeinPeer*> peerTable;

  /**
   * @brief processAsyncEntityUpdate
   * @param pMessage
   */
  void processAsyncEntityUpdate(protobuf::VeinProtocol *pMessage);

  /**
   * @brief processAsyncPeerlistUpdate
   * @param pMessage
   */
  void processAsyncPeerlistUpdate(protobuf::VeinProtocol *pMessage);

  /**
   * @brief processAsyncPeerUpdate
   * @param pMessage
   */
  void processAsyncPeerUpdate(protobuf::VeinProtocol *pMessage);

  /**
   * @brief processRequestEntityData
   * @param pMessage
   */
  void processRequestEntityData(protobuf::VeinProtocol *pMessage);

  /**
   * @brief processRequestEntityStartAsyncUpdate
   * @param pMessage
   */
  void processRequestEntityStartAsyncUpdate(protobuf::VeinProtocol *pMessage);

  /**
   * @brief processRequestEntityStopAsyncUpdate
   * @param pMessage
   */
  void processRequestEntityStopAsyncUpdate(protobuf::VeinProtocol *pMessage);
  /**
   * @brief processRequestPeerlist
   * @param pMessage
   */
  void processRequestPeerlist(protobuf::VeinProtocol *pMessage);

  /**
   * @brief processRequestPeerData
   * @param pMessage
   */
  void processRequestPeerData(protobuf::VeinProtocol *pMessage);

  /**
   * @brief processMessageDebug
   * @param pMessage
   */
  void processMessageDebug(protobuf::VeinProtocol *pMessage);

  /**
   * @brief processMessageError
   * @param pMessage
   */
  void processMessageError(protobuf::VeinProtocol *pMessage);

  /**
   * @brief processInitHub
   * @param pMessage
   */
  void processInitHub(protobuf::VeinProtocol *pMessage);

  /**
   * @brief processKillHub
   * @param pMessage
   */
  void processKillHub(protobuf::VeinProtocol *pMessage);


  /**
   * @brief If the hub is initialized then it is a networked hub that should not be changed locally
   */
  bool hubInitialized;
  QUuid hubUuid;



  Q_DISABLE_COPY(VeinHub)
};

#endif // VEINMANAGER_H
