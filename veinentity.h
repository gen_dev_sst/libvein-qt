#ifndef VEINDATA_H
#define VEINDATA_H

#include "libveinqt_global.h"

#include <QObject>
#include <QVariant>
#include <QUuid>



class VeinAuth;
class VeinHub;
class VeinPeer;

namespace google
{
  namespace protobuf
  {
    class Message;
  }
}

/**
 * @brief Data container class
 */
class LIBVEINQTSHARED_EXPORT VeinEntity : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QVariant value READ getValue WRITE setValue NOTIFY sigValueChanged)
  Q_PROPERTY(QString name READ getName)
  Q_PROPERTY(QList<Modifiers> modifiers READ getModifiers NOTIFY sigModifiersChanged)
  Q_ENUMS(Modifiers)

public:
  /**
   * @todo Replace friend class with accessing gateway
   */
  friend class ::VeinHub;

  /**
   * @brief Modifiers like const
   */
  enum Modifiers {
    MOD_CONST = 0, /**< @brief Data will not change */
    MOD_READONLY, /**< @brief Writing the data is only possible for the issuing VeinPeer */
    MOD_NOECHO /**< @brief Will not emit updated if the value has been set to the actual value twice */
  };

  /**
   * @brief Constructor called from the belonging peer
   * @param[in] peer The owner of this VeinEntity
   * @param name Descriptor of this entity
   * @note should not be called for remote peers
   */
  VeinEntity(VeinPeer* peer, QString name);

  VeinEntity(const QUuid& uuid, VeinPeer* peer, google::protobuf::Message *pMessage);

  /**
   * @brief Returns the VeinHub this entity (and its owning VeinPeer) belongs to
   * @return
   */
  VeinHub *getHub();

  /**
   * @brief Returns active Modifiers
   * @return All Modifiers of the VeinEntity
   */
  QList<Modifiers> getModifiers();

  /**
   * @brief The name of this entity
   * @return QString
   * @note Entity names must not be ambiguous!
   */
  QString getName();

  /**
   * @brief getOwner
   * @return
   */
  VeinPeer *getOwner();

  QList<QUuid> getRemoteConnections();
  /**
   * @brief getUID
   * @return
   */
  const QUuid &getUuid();

  /**
   * @brief Getter for the actual data
   * @return some data is returned or QVariant::Invalid
   */
  QVariant getValue();


signals:

  /**
   * @brief Notifies remote hubs on updates of this entity
   */
  void sigAsyncUpdatesStart();

  /**
   * @brief Stops update notifications sent via protobuf
   */
  void sigAsyncUpdatesStop();

  /**
   * @brief Will be emitted on network errors, or if the value has been removed from introspection
   */
  void sigErrorValueNotAvailable();

  /**
   * @brief modifierChanged
   */
  void sigModifiersChanged(QList<Modifiers> mod);

  /**
   * @brief protobufModifiersChanged
   * @param pMessage
   */
  void sigProtobufModifiersChanged(google::protobuf::Message *pMessage);

  /**
   * @brief Will be emitted if data changes, or events are transmitted
   *
   * @note Events are values of the type void, they are used like a Qt signal with no parameters
   */
  void sigValueChanged(QVariant val);

  /**
   * @brief The VeinHub will use this signal to send it to all interested other hubs
   * @param pMessage
   */
  void sigProtobufValueChanged(google::protobuf::Message *pMessage);



public slots:
  /**
   * @brief Sets the value if MOD_READONLY is not present, or issuer is the parent of this Entity
   * @param newData The value to be written
   * @param issuer Only needed when the value is readonly
   */
  void setValue(QVariant newData, VeinPeer *issuer=0);

  /**
   * @brief Will be called from VeinHub on remote value changes
   * @param newData
   */
  void setValueExtern(QVariant newData);

  /**
   * @brief This slot will fetch the entity data again
   */
  void fetchEntityData();

  /**
   * @brief Adds the modifier if not already set
   * @param mod Modifier to be added
   * @return true for success
   */
  bool modifiersAdd(Modifiers mod);

  /**
   * @brief Empties the list of Modifiers
   *
   * @todo Only to be called behind an access gateway class
   */
  bool modifiersClear();

  /**
   * @brief Removes the modifier if set
   * @param mod Modifier to be removed
   * @return true for success
   */
  bool modifiersRemove(Modifiers mod);



protected slots:
  /**
   * @brief Called via sigAsyncUpdatesStart()
   *
   * Sends the protobuf::VeinProtocol over the VeinHub to start asynchrounous updates
   */
  void onAsyncUpdateStarted();
  /**
   * @brief Called via sigAsyncUpdatesStop()
   *
   * Sends the protobuf::VeinProtocol over the VeinHub to stop asynchrounous updates
   */
  void onAsyncUpdateStopped();

  /**
   * @brief Will be called when asynchronous updates are started on the side of another application using libvein
   * @param[in] rConnection Identifier for the connection
   */
  void onRemoteConnectionAdded(const QUuid &rConnection);

  /**
   * @brief Will be called when asynchronous updates are stopped on the side of another application using libvein
   * @param[in] rConnection Identifier for the connection
   */
  void onRemoteConnectionRemoved(const QUuid &rConnection);

  /**
   * @brief Discards old modifiers and sets them to the new list
   * @param mod The new modifiers
   */
  void onRemoteModifersChanged(QList<Modifiers> mod);

  /**
   * @brief Will always succeed, VeinHub calls this when the value has been changed over protobuf
   * @param newData
   */
  void onRemoteValueChanged(QVariant newData);

private:
  /**
   * @brief See Qt documentation
   * @param qmmSignal
   */
  void connectNotify(const QMetaMethod &qmmSignal) Q_DECL_OVERRIDE;

  /**
   * @brief See Qt documentation
   * @param qmmSignal
   */
  void disconnectNotify(const QMetaMethod & qmmSignal) Q_DECL_OVERRIDE;

  void setupSigSlots();

  /**
   * @brief Setter for the actual data
   * @param newData The value to be written
   * @param[in] issuer For permission checking
   */
  void setRealValue(QVariant newData, VeinPeer *issuer);

  //pimpl
  /**
   * @brief Authentification requirements
   * @todo PIMPL
   */
  VeinAuth *auth;

  /**
   * @brief name
   * @todo PIMPL
   */
  QString entityName;

  /**
   * @brief Identifiers that are unique within the peer the entity belongs to
   * @todo PIMPL
   */
  QUuid entityUuid;

  /**
   * @brief owner
   * @todo PIMPL
   */
  VeinPeer *owner;

  /**
   * @brief entityModifiers
   * @todo PIMPL
   */
  QList<Modifiers> entityModifiers;


  /**
   * @brief The actual value
   * @todo PIMPL
   */
  QVariant data;

  /**
   * @brief Tracks whether this entity is (locally) used or not
   */
  qint32 connectionCount;

  /**
   * @brief Tracks the connections that are not local
   */
  QList<QUuid> remoteConnections;



  Q_DISABLE_COPY(VeinEntity)
};

#endif // VEINDATA_H
