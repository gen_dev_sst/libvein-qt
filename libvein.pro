#-------------------------------------------------
#
# Project created by QtCreator 2013-10-21T10:49:03
#
#-------------------------------------------------
include(../include/project-paths.pri)


QT       -= gui

TARGET = vein-qt
TEMPLATE = lib

DEFINES += VEINQT_LIBRARY

SOURCES += \
    veinpeer.cpp \
    veinentity.cpp \
    veinauth.cpp \
    veinhub.cpp

HEADERS +=\
    veinpeer.h \
    veinentity.h \
    veinauth.h \
    libveinqt_global.h \
    veinhub.h

INCLUDEPATH += $$VEIN_PROTOBUF_INLCUDEDIR

LIBS += $$VEIN_PROTOBUF_LIBDIR -lvein-qt-protobuf
LIBS += -lprotobuf

QMAKE_CXXFLAGS += -Wall -Wshadow -Wfloat-equal

header_files.files = $$HEADERS
header_files.path = /usr/include
INSTALLS += header_files

target.path = /usr/lib
INSTALLS += target
